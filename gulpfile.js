// README:
// This project is set up to compile individual scss files to css files.
// You must add your individual css files to the header for it to be included
// Please try to include your files only on pages they must be included on
// I have removed js concatenation

var gulp = require('gulp');
var sass = require('./node_modules/gulp-sass');
var sourcemaps = require('./node_modules/gulp-sourcemaps');
var autoprefixer = require('./node_modules/gulp-autoprefixer');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var FTPdelay = 500;

// This function will take a .scss file, create a sourcemap, run Autoprefixer on it, and then write to the public/css folder
gulp.task('sass', function () {
  setTimeout(function() {
    gulp.src('public/scss/*.scss')
      .pipe(sass({includePaths: ['public/scss/partials']}))
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false,
        remove: true
      }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('public/css/'));
  }, FTPdelay);
});

gulp.task('js', function() {
  setTimeout(function(){
    return gulp.src(['public/js/*.js'])
      .pipe(concat('all.js'))
      .pipe(gulp.dest('public/js/dist/'))
  }, FTPdelay);
});

// This is the task that will run every time you run "gulp" after everything is installed.
gulp.task('default', function () {
  gulp.start('sass', 'js');
  gulp.watch('public/scss/**/*.scss', ['sass']);
  gulp.watch('public/js/*.js', ['js']);
});
