<!DOCTYPE html>
<html ng-app="blogTest">
  <head>
    <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Blog Test</title>
  <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">
  <link rel="stylesheet" href="public/css/styles.css" type="text/css">
  </head>
  <body>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js" type="text/javascript"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-route.js"></script>

    <div ng-view>
        <edit-bar></edit-bar>
        <div class="posts_container">
            <post-manager></post-structure>
        </div>
    </div>
  <!-- Local -->
  <script src="public/js/dist/all.js" type="text/javascript"></script>

  
  
  </body>
</html>

