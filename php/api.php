<?php
  require 'db_conn.php';
  

  $method = $_SERVER['REQUEST_METHOD'];
  $request = explode('/', trim($_SERVER['PATH_INFO'],'/'));
  $input = json_decode(file_get_contents('php://input'),true);

  switch ($method) {
    case 'GET':
      get_posts();
    break;
    case 'DELETE':
        $id = $_REQUEST['post_id'];
        delete_post($id);
    break;
    case 'POST':
      $title = mysqli_real_escape_string($conn, $_REQUEST['title']);
      $type = mysqli_real_escape_string($conn, $_REQUEST['content_type']);
      $content = mysqli_real_escape_string($conn, $_REQUEST['content']);
      $tStamp = mysqli_real_escape_string($conn, $_REQUEST['post_date']);
      add_post($title, $type, $content, $tStamp);
    break;
  }

  function get_posts() {
    global $conn;
    $sql = "SELECT * FROM posts";
    $result = $conn->query($sql);
    
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    
    echo json_encode($emparray);
    
    $conn->close();
  }
  
  function add_post($title, $type, $content, $tStamp) {
    global $conn;
    $sql = "INSERT INTO posts (title,content_type,content, post_date) VALUES ('" . $title . "', '" . $type . "', '" . $content . "', '" . $tStamp . "')";
    
    if ($conn->query($sql) === TRUE) {
      
    } else {
      echo "Error: " . $sql . "<br>" . $conn->error;
    }
  }
  
  function delete_post($id) {
    global $conn;
    $sql = "DELETE FROM posts WHERE id=" . $id;
    
    if ($conn->query($sql) === TRUE) {
      echo 1;
    }
    else {
      echo 'err';
    }
  }

?>
