app.directive('addForm', function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/addform-structure.html',
    controller: function($scope, $timeout, $sce) {
      var addForm = this;
      

      // PUBLIC PROPERTIES

      // Object that holds the content for posts being created
      addForm.newPost; 
      

      // PUBLIC METHODS

      // Resets the form, resets the new post object,
      // and gets the posts
      addForm.resetFormAndPosts = function() {
        setFormPristine();
        resetNewPost();
        getPosts();
      };
      
      // Returns the video ID from a youtube URL entered
      addForm.getYoutubeID = function(newValue) {
        var embedString = 'https://www.youtube.com/embed/',
        vidID = newValue.indexOf('youtu.be/'),
        vidID2 = newValue.indexOf('watch?v=');

        if( vidID != -1 ) {
          vidID += 9;
        }
        else if ( vidID2 !== -1 ) {
          vidID = vidID2 + 8;
        }
        vidID = newValue.substr(vidID);
        return embedString + vidID;
      };


      //  PRIVATE METHODS

      // Sets the add post form to pristine
      function setFormPristine() {
        $scope.add_post.$setPristine();
      }

      // Resets the newPost object
      function resetNewPost() {
        addForm.newPost = {
          type: "text",
          content: '',
          textContent: '',
          imageContent: '',
          videoContent: '',
          post_date: Date.now()
        };
      }
      
      // Calls the post manager to update the posts
      function getPosts() {
        $scope.blog.getPosts();
      }

      // Init
      (function(){
        // When the post type changes, this is called
        $scope.$watch("addForm.newPost.type", function( newValue ) {
            addForm.newPost.typeContentSelector = newValue+"Content";
        });
        
        // When the video URL changes, grab just the video id
        $scope.$watch("addForm.newPost.videoContent", function( newValue ) {
            if( newValue ) {
                var vidURL = addForm.getYoutubeID(newValue);
                addForm.newPost.content = $sce.trustAsResourceUrl(vidURL);
            }
        });

        resetNewPost();

      })();
    },
    controllerAs: 'addForm'
  };
});