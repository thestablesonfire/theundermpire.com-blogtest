app.directive('editBar', function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/editbar-structure.html',
    controller: function($scope, $document, $timeout) {
        var editBar = this;


        // PUBLIC PROPERTIES
        
        editBar.showForm = false;


        // PUBLIC METHODS

        // Passes the post information from the addForm to the postController
        editBar.addPost = function() {
            var formObject = $scope.addForm.newPost,
            submitData = {
                content: formObject.content,
                title: formObject.title,
                type: formObject.type,
            },
            typeContentSelector = submitData.type + "Content";

            // Here we need to add a property for the type of post it is
            submitData[typeContentSelector] = formObject[typeContentSelector];

            var params = {
                title: submitData.title,
                content_type: submitData.type,
                content: submitData[typeContentSelector],
                post_date: Date.now()
            };
            $scope.blog.addPost(params);
        };

        // Public method to hide the form
        editBar.hideForm = function() {
            $timeout(function(){
                editBar.showForm = false;
            });
        };

    },
    controllerAs: 'editBarCtrl'
  };
});