var app = angular.module('blogTest', []);
app.directive('addForm', function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/addform-structure.html',
    controller: function($scope, $timeout, $sce) {
      var addForm = this;
      

      // PUBLIC PROPERTIES

      // Object that holds the content for posts being created
      addForm.newPost; 
      

      // PUBLIC METHODS

      // Resets the form, resets the new post object,
      // and gets the posts
      addForm.resetFormAndPosts = function() {
        setFormPristine();
        resetNewPost();
        getPosts();
      };
      
      // Returns the video ID from a youtube URL entered
      addForm.getYoutubeID = function(newValue) {
        var embedString = 'https://www.youtube.com/embed/',
        vidID = newValue.indexOf('youtu.be/'),
        vidID2 = newValue.indexOf('watch?v=');

        if( vidID != -1 ) {
          vidID += 9;
        }
        else if ( vidID2 !== -1 ) {
          vidID = vidID2 + 8;
        }
        vidID = newValue.substr(vidID);
        return embedString + vidID;
      };


      //  PRIVATE METHODS

      // Sets the add post form to pristine
      function setFormPristine() {
        $scope.add_post.$setPristine();
      }

      // Resets the newPost object
      function resetNewPost() {
        addForm.newPost = {
          type: "text",
          content: '',
          textContent: '',
          imageContent: '',
          videoContent: '',
          post_date: Date.now()
        };
      }
      
      // Calls the post manager to update the posts
      function getPosts() {
        $scope.blog.getPosts();
      }

      // Init
      (function(){
        // When the post type changes, this is called
        $scope.$watch("addForm.newPost.type", function( newValue ) {
            addForm.newPost.typeContentSelector = newValue+"Content";
        });
        
        // When the video URL changes, grab just the video id
        $scope.$watch("addForm.newPost.videoContent", function( newValue ) {
            if( newValue ) {
                var vidURL = addForm.getYoutubeID(newValue);
                addForm.newPost.content = $sce.trustAsResourceUrl(vidURL);
            }
        });

        resetNewPost();

      })();
    },
    controllerAs: 'addForm'
  };
});
app.directive('editBar', function() {
  return {
    restrict: 'E',
    templateUrl: 'templates/editbar-structure.html',
    controller: function($scope, $document, $timeout) {
        var editBar = this;


        // PUBLIC PROPERTIES
        
        editBar.showForm = false;


        // PUBLIC METHODS

        // Passes the post information from the addForm to the postController
        editBar.addPost = function() {
            var formObject = $scope.addForm.newPost,
            submitData = {
                content: formObject.content,
                title: formObject.title,
                type: formObject.type,
            },
            typeContentSelector = submitData.type + "Content";

            // Here we need to add a property for the type of post it is
            submitData[typeContentSelector] = formObject[typeContentSelector];

            var params = {
                title: submitData.title,
                content_type: submitData.type,
                content: submitData[typeContentSelector],
                post_date: Date.now()
            };
            $scope.blog.addPost(params);
        };

        // Public method to hide the form
        editBar.hideForm = function() {
            $timeout(function(){
                editBar.showForm = false;
            });
        };

    },
    controllerAs: 'editBarCtrl'
  };
});
  app.directive('postManager', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/post-structure.html',
      controller: function($scope, $window, $timeout, $sce) {
        var postCtl = this,
        postDebug = false;


        // PUBLIC PROPERTIES

        postCtl.posts = [ [], [], [], [] ]; // Holds posts in numColumnsActive columns


        // PRIVATE PROPERTIES

        var numColumnsActive = 1,   // holds active # of columns
        // Post dimensions - constants
        postWidth = 280,
        marginWidth = 10,
        totalPostWidth = postWidth + (2*marginWidth),
        // Holds all post data
        postData = [];
        

        // PUBLIC METHODS

        // If the number of columns should change, this sets that number
        // Then calls the function that re-sorts the posts
        postCtl.setNumColumns = function(numColumns) {
          postCtl.numColumnsActive = numColumns;
          if(postData)sortPosts();
        };
        
        // Checks the window width against column breakpoints and
        // Calls the setNumColumns function if column number changes
        postCtl.windowResize = function() {
          var width = postCtl.w.width(),
          numColumns = 1;
          
          if( width >= totalPostWidth * 4 ) {
            numColumns = 4;
          } else if( width < totalPostWidth * 4 && width >= totalPostWidth * 3 ) {
            numColumns = 3;
          } else if( width < totalPostWidth * 3 && width >= totalPostWidth * 2 ) {
            numColumns = 2;
          }
          if( numColumns != postCtl.numColumnsActive ) {
            postCtl.setNumColumns(numColumns);
          }
        };

        // This piggy-backs off of the addForm's functionality that returns a valid youTube URL
        // We're using it for the actual post here, where the addForm uses it for video previews
        postCtl.getPostYoutubeVideoId = function(post) {
          var src = $sce.trustAsResourceUrl($scope.addForm.getYoutubeID(post.content));
          return src;
        };

        // Get all posts
        postCtl.getPosts = function() {
            var obj = {getPosts: 1},
            postURI = createRestURI(obj),
            request = makeRestCall(postURI, "GET");

            request.done(function(data) {
              var jsonResult = JSON.parse(data);
              postData = jsonResult;
              sortPosts();
            }).error(function(e) {
              console.log(e);
            });
        };

        // Adds a post passed in from the editBar
        postCtl.addPost = function(params) {
            var postURI = createRestURI(params),
            request = makeRestCall(postURI, "POST");
            request.done(function (response, textStatus, jqXHR) {
                if(postDebug)console.log("Yes", response);
                $scope.editBarCtrl.hideForm();
                $scope.addForm.resetFormAndPosts();
            });
            request.fail(function (jqXHR, textStatus, errorThrown) {
                console.error("The following error occurred: " + textStatus, errorThrown);
            });
        };

        // Delete a single post
        postCtl.deletePost = function(post) {
            var obj = {post_id: post.id};
            postURI = createRestURI(obj),
            request = makeRestCall(postURI, "DELETE");
            request.done(function (response, textStatus, jqXHR) {
                if(postDebug)console.log(response);
                postCtl.getPosts();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.error("The following error occurred: " + textStatus, errorThrown);
            });
        };
        
        
        // PRIVATE METHODS

        // Bind the jQuery window object and then bind the resize function
        function bindResize() {
            postCtl.w = angular.element($window);
            postCtl.w.bind('resize', function() {
                postCtl.windowResize();
            });
        }
        
        // Sorts all of the posts into the total number of columns
        function sortPosts() {
          var newPosts = [ [], [], [], [] ],
          index = 0;
          for ( var post = postData.length - 1; post >= 0; post--) {
            newPosts[index % postCtl.numColumnsActive].push(postData[post]);
            index++;
          }
          // Apply changes to scope
          $timeout(function(){
            postCtl.posts = newPosts;
          })
        }

        // Bind window resize function,
        // get post data
        // set columns based on width
        function init() {
            bindResize();
            postData = postCtl.getPosts();
            postCtl.windowResize();
        }


        // REST METHODS

        // Builds a rest uri from a passed in object
        function createRestURI(params) {
            var postURI = "php/api.php/",
            first = true;
            for(var param in params) {
                if(first) {
                    postURI += '?';
                    first = false;
                }
                else postURI += '&';
                postURI += param + "=" + params[param];
            }
            return encodeURI(postURI);
        };

        function makeRestCall(rURI, rType) {
            return request = $.ajax({
                url: rURI,
                type: rType
            });
        };


        init();
      },
      controllerAs: 'blog'
    };
  });