var app = angular.module('blogTest', [ ]);

  app.directive('postStructure', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/post-structure.html',
      controller: function($scope, $window, $timeout) {
        var postCtl = this;
        
        // This is the structure that holds the posts
        postCtl.posts = [ [], [], [], [] ];

        var postData = getPosts();
        var numColumnsActive = 1;
        
        // Post dimensions
        var postWidth = 280;
        var marginWidth = 10;
        var totalPostWidth = postWidth + (2*marginWidth);
        
        
        // ========== PUBLIC METHODS ========== //
        
        // If the number of columns should change, this sets that number
        // Then calls the function that re-sorts the posts
        postCtl.setNumColumns = function(numColumns) {
          postCtl.numColumnsActive = numColumns;
          sortPosts();
        };
        
        // Checks the window width against column breakpoints and
        // Calls the setNumColumns function if column number changes
        postCtl.windowResize = function() {
          var width = w.width();
          var numColumns = 1;
          
          if( width >= totalPostWidth * 4 ) {
            numColumns = 4;
          } else if( width < totalPostWidth * 4 && width >= totalPostWidth * 3 ) {
            numColumns = 3;
          } else if( width < totalPostWidth * 3 && width >= totalPostWidth * 2 ) {
            numColumns = 2;
          }
          if( numColumns != postCtl.numColumnsActive ) {
            postCtl.setNumColumns(numColumns);
          }
        };
        
        
        // ========== PRIVATE METHODS ========== //
        
        // Bind the jQuery window object and then bind the resize function
        var w = angular.element($window);
        w.bind('resize', function() {
          postCtl.windowResize();
        });
        
        function sortPosts() {
          var newPosts = [ [], [], [], [] ];
          for ( var post in postData ) {
            newPosts[post % postCtl.numColumnsActive].push(postData[post]);
          }
          $timeout(function(){
            postCtl.posts = newPosts;
          })
        }
        
        function getPosts() {
          $.ajax({ url: 'php/post_actions.php',
            data: {action: 'get_posts'},
            type: 'post',
            success: function(data) {
              var jsonResult = JSON.parse(data);
              postData = jsonResult;
              sortPosts();
            },
            error: function(e) {
              console.log(e);
            }
          });
        }
        
        postCtl.windowResize();
      },
      controllerAs: 'blog'
    };
  });